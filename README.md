# CashRegister

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.1.

Ce site contient 3 éléments:
 - un header central dans le app.component pour la navigation et garder l'information du montant total TTC en visuel
 - une page inventaire pour lister les objets à ajouter au panier
 - une page panier pour consulter la facture et procéder au paiement

j'ai utilisé du bootstrap et ng-bootstrap en librairie.

Les page sont en lazy loading pour ne pas charger les contenus non visible dans les sources du navigateur.

J'ai utilisé cette syntaxe qui se existe après compilation car elle présente plusieurs avantages, éviter les fuites mémoire, éviter de gérer le cycle de vie des observables et éviter d'appeler plusieur fois un meme observable dans le cas où l'appel se fait plusieur endroit dans la même page.
<ng-container
*ngTemplateOutlet="totoTemplate; context: { machin: $machin | async }"></ng-container>
<ng-template #totoTemplate let-articles="machin"></ng-template>


Le site est perfectible mais il fait ce qu'on attend de lui.

Si j'avais un peu plus de temps:
- j'ajouterai les produits avec un nombre d'articles définis plutot qu'un par un.
- j'ajouterai un plus et un moins sur chaque ligne de la facture pour ajuster dynamiquement les quantités de produits.