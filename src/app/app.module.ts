import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TotalTtcPipe } from "./total-ttc.pipe";
import { ToastService } from "./common/services/toast.service";
import { ToastsComponent } from "./common/components/toasts.component";

@NgModule({
    declarations: [
        AppComponent,
        TotalTtcPipe
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        NgbModule,
        ToastsComponent
    ],
    providers: [ToastService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
