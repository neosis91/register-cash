import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inventory } from "../common/entities/inventory.entity";

@Injectable({
    providedIn: 'root'
})
export class InventoryService {
    private productsUrl = 'assets/products.json';

    constructor(private http: HttpClient) {
    }

    getInventory(): Observable<Inventory> {
        return this.http.get<Inventory>(this.productsUrl);
    }
}