import { Pipe, PipeTransform } from '@angular/core';
import { Product } from "../common/entities/product.entity";

@Pipe({
    name: 'priceTTC'
})
export class PriceTtcPipe implements PipeTransform {
    transform(product: Product): number {
        if (product?.price > 0) {
            return product.price + (product.price * (product.tva / 100));
        }
        return 0;
    }
}
