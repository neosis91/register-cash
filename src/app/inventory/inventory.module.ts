import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryComponent } from "./inventory.component";
import { RouterModule } from "@angular/router";
import { INVENTORY_ROUTE } from "./inventory.route";
import { InventoryService } from "./inventory.service";
import { NgbNav, NgbNavContent, NgbNavItem, NgbNavLinkButton, NgbNavOutlet } from "@ng-bootstrap/ng-bootstrap";
import { PriceTtcPipe } from "./price-ttc.pipe";

@NgModule({
    declarations: [InventoryComponent, PriceTtcPipe],
    imports: [
        CommonModule,
        RouterModule.forChild(INVENTORY_ROUTE),
        NgbNav,
        NgbNavLinkButton,
        NgbNavOutlet,
        NgbNavContent,
        NgbNavItem
    ],
    providers: [InventoryService]
})
export class InventoryModule {
}
