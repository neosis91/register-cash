import { Routes } from '@angular/router';
import { InventoryComponent } from "./inventory.component";

export const INVENTORY_ROUTE: Routes = [
    {
        path: '',
        component: InventoryComponent
    }
];


