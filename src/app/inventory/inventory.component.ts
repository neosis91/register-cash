import { Component } from '@angular/core';
import { Product } from "../common/entities/product.entity";
import { InventoryService } from "./inventory.service";
import { Observable } from "rxjs";
import { Inventory } from "../common/entities/inventory.entity";
import { CartService } from "../common/services/cart.service";
import { ToastService } from "../common/services/toast.service";
import { SelectedProduct } from "../cart/cart.entity";

@Component({
    selector: 'app-cash',
    templateUrl: './inventory.component.html'
})
export class InventoryComponent {
    $inventory: Observable<Inventory>;
    $articles: Observable<SelectedProduct[]>;
    active = 'Boissons';
    cart: Product[] = [];

    constructor(private productService: InventoryService,
                private toastService: ToastService,
                private cartService: CartService) {
        this.$inventory = this.productService.getInventory();
        this.$articles = this.cartService.$articles;
    }

    addProducts(product: Product) {
        this.cartService.addProduct(product);
        this.toastService.show(`${product.name} a été ajouté au panier`, {
            classname: 'bg-success text-light',
            delay: 5000
        });
    }
}
