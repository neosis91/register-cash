import { Component } from '@angular/core';
import { CartService } from "./common/services/cart.service";
import { Observable } from "rxjs";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'cash-register';
    $articles: Observable<any>;

    constructor(private cartService: CartService) {
        this.$articles = cartService.$articles;
    }
}
