import { Category } from "./categories.entity";

export interface Inventory {
    categories: Category[];
}