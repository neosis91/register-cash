import { Injectable } from '@angular/core';
import { Product } from "../entities/product.entity";
import { BehaviorSubject, Observable } from "rxjs";
import { SelectedProduct } from "../../cart/cart.entity";

@Injectable({
    providedIn: 'root'
})
export class CartService {
    private articles = new BehaviorSubject<SelectedProduct[]>([]);
    public $articles: Observable<SelectedProduct[]> = this.articles.asObservable();

    addProduct(product: Product): void {
        const currentCart = this.articles.value;
        const idxSP = currentCart.findIndex(selectedProduct => selectedProduct.product.name === product.name);
        if (idxSP >= 0) {
            currentCart[idxSP].addOneProduct();
        } else {
            const newSelectedProduct = new SelectedProduct(product);
            currentCart.push(newSelectedProduct);
        }
        this.articles.next(currentCart);
    }

    removeAllProduct(): void {
        this.articles.next([]);
    }
}