import { Pipe, PipeTransform } from '@angular/core';
import { SelectedProduct } from "./cart/cart.entity";

@Pipe({
    name: 'totalTTC',
    pure: false
})
export class TotalTtcPipe implements PipeTransform {
    transform(articles: SelectedProduct[]): string | number {
        if (articles?.length > 0) {
            return [...articles].reduce(
                (pre, cur) => {
                    if (cur) {
                        pre += cur.TTC;
                    }
                    return pre;
                },
                0 as number
            ).toFixed(2);
        }
        return 0;
    }
}
