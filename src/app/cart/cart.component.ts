import { Component } from '@angular/core';
import { map, Observable } from "rxjs";
import { CartService } from "../common/services/cart.service";
import { SelectedProduct } from "./cart.entity";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastService } from "../common/services/toast.service";

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent {
    $articles: Observable<SelectedProduct[]>;
    total: { HT: number | string, TTC: number | string } = {HT: 0, TTC: 0};

    constructor(private cartService: CartService, private modalService: NgbModal, private toastService: ToastService) {
        this.$articles = this.cartService.$articles.pipe(
            map(articles => {
                if (articles.length > 0) {
                    this.total = [...articles].reduce(
                        (pre, cur) => {
                            if (cur) {
                                pre.HT += cur.HT;
                                pre.TTC += cur.TTC;
                            }
                            return pre;
                        },
                        this.total as { HT: number, TTC: number }
                    );
                    this.total.HT = Number(this.total.HT).toFixed(2);
                    this.total.TTC = Number(this.total.TTC).toFixed(2);
                }
                return articles;
            })
        );
    }

    cancel(): void {
        this.cartService.removeAllProduct();
    }

    paid(content: any): void {
        const modal = this.modalService.open(content, {centered: true});
        modal.result.then((result: string) => {
            if (result === 'Payer') {
                this.cartService.removeAllProduct();
                this.toastService.show('Paiement réalisé avec succès!', {
                    classname: 'bg-success text-light',
                    delay: 5000
                });
            }
        });
    }
}
