import { Product } from "../common/entities/product.entity";

export class SelectedProduct {
    nb = 1;
    product: Product;

    constructor(product: Product) {
        this.product = product;
    }

    get HT(): number {
        return this.nb * this.product.price;
    }

    get TTC(): number {
        const HT = this.nb * this.product.price;
        return HT + (HT * (this.product.tva / 100));
    }

    addOneProduct() {
        this.nb++;
    }
}