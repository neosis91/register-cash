import { Routes } from '@angular/router';
import { CartComponent } from "./cart.component";

export const CART_ROUTE: Routes = [
    {
        path: '',
        component: CartComponent
    }
];


