import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { RouterModule } from "@angular/router";
import { CART_ROUTE } from "./cart.route";

@NgModule({
    declarations: [
        CartComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(CART_ROUTE)
    ]
})
export class CartModule {
}
